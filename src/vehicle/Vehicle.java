package vehicle;

import vehicle.exceptions.EngineNotStartedException;
import vehicle.exceptions.EngineNotStoppedException;
import vehicle.exceptions.InvalidGearException;
import vehicle.exceptions.InvalidProductionYearException;
import vehicle.exceptions.InvalidSpeedException;
import vehicle.exceptions.VehicleException;
import vehicle.exceptions.VehicleRuntimeException;

public abstract class Vehicle implements VehicleInterface {

	private int currentGear;

	private int currentSpeed;

	private int productionYear;

	private Color color;

	private Vehicle() {
		this(-1, -1);
	}

	private Vehicle(int currentGear, int currentSpeed) {
		this.currentGear = currentGear;
		this.currentSpeed = currentSpeed;
	}

	public Vehicle(Color color, int productionYear) {
		this();
		try {
			this.color = color;
			setProductionYear(productionYear);
		} catch (VehicleException e) {
			throw new VehicleRuntimeException("Couldn't create a vehicle instance", e);
		}
	}

	private void setProductionYear(int productionYear) throws InvalidProductionYearException {
		int productionEndYear = getProductionEndYear();
		int productionStartYear = getProductionStartYear();

		if (productionStartYear > productionYear || productionEndYear < productionYear) {
			throw new InvalidProductionYearException(
					"The production year must be between " + productionStartYear + " and " + productionEndYear);
		} else {
			this.productionYear = productionYear;
		}

	}

	public String getColor() {
		return this.color.toString();
	}

	public int getProductionYear() {
		return this.productionYear;
	}

	public void changeGear(int newGear) throws InvalidGearException {
		if (newGear >= 0 && newGear <= getNumberOfGears()) {
			if (newGear < this.currentGear) {
				// check that the current speed is correct for the gear.
				int maxSpeedForGear = getMaxSpeedForGear(newGear);
				if (maxSpeedForGear < this.currentSpeed) {
					throw new InvalidGearException("You can't change the gear back to " + newGear
							+ " before you slow down to " + maxSpeedForGear + " KM/h");
				}
			}
			this.currentGear = newGear;
		} else {
			throw new InvalidGearException(
					"The vehicle only has " + getNumberOfGears() + " gears, you can't set the gear to " + newGear);
		}
	}

	public void accelerate(int acceleration)
			throws EngineNotStartedException, InvalidGearException, InvalidSpeedException {
		if (acceleration < 0) {
			throw new InvalidSpeedException("The speed must be positive");
		}

		int tempSpeed = this.currentSpeed + acceleration;

		// check if the engine is started
		if (this.currentGear < 0) {
			throw new EngineNotStartedException(
					"You can't accelerate before starting the engine. Make sure the engine is started.");
		}

		// the maximum speed that can be achieved in the current gear
		int maxSpeed = getMaxSpeedForGear(this.currentGear);

		if (this.currentGear == 0) {
			// check if the gear was set
			throw new InvalidGearException("You can't accelerate without seting a gear. Please set a gear > 0 first.");
		} else if (tempSpeed <= maxSpeed) {
			// The happy use case when the vehicle actually accelerates
			System.out.println("The vehicle reached the speed of " + tempSpeed + " KM/h");
			this.currentSpeed = tempSpeed;
		} else {
			this.currentSpeed = maxSpeed;
			throw new InvalidSpeedException("The speed you will reach by acceleration by " + acceleration
					+ " will be over the maximum speed allowed in the current gear (" + this.currentGear
					+ "), which is " + maxSpeed);
		}
	}

	public void startEngine() throws EngineNotStartedException {
		System.out.println("Starting the engine... ");
		if (this.currentGear > 0 || this.currentSpeed > 0) {
			throw new EngineNotStartedException("The engine was not started again because it was not stopped.");
		} else {
			this.currentGear = 0;
			this.currentSpeed = 0;
			System.out.println(toString() + ": Engine started.");
		}
	}

	public void stopEngine() throws EngineNotStoppedException {
		if (this.currentGear < 0 || this.currentSpeed < 0) {
			throw new EngineNotStoppedException("You can't stop the engine since it's not started.");
		} else {
			System.out.println(toString() + ": Engine stoped.");
			this.currentGear = -1;
			this.currentSpeed = -1;
		}

	}

	public String toString() {
		return getProducer() + " " + getModel() + " " + getColor();
	}

}
