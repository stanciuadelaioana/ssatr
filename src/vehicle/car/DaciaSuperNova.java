package vehicle.car;

import vehicle.Color;
import vehicle.exceptions.InvalidGearException;

public class DaciaSuperNova extends Dacia {

	public static final int MAX_SPEED = 180;

	public static final String MODEL = "SuperNova";

	public static final int PRODUCTION_START_YEAR = 2010;

	public static final int PRODUCTION_END_YEAR = 2016;

	public static final int NUMBER_OF_GEARS = 5;

	private static final int[] GEAR_RANGES = { 0, 20, 40, 80, 120, 180 };

	public DaciaSuperNova(Color color, int productionYear) {
		super(color, productionYear);
	}

	@Override
	public int getMaxSpeed() {
		return DaciaSuperNova.MAX_SPEED;
	}

	@Override
	public String getModel() {
		return DaciaSuperNova.MODEL;
	}

	@Override
	public int getProductionStartYear() {
		return DaciaSuperNova.PRODUCTION_START_YEAR;
	}

	@Override
	public int getProductionEndYear() {
		return DaciaSuperNova.PRODUCTION_END_YEAR;
	}

	@Override
	public int getNumberOfGears() {
		return DaciaSuperNova.NUMBER_OF_GEARS;
	}

	@Override
	public int getMaxSpeedForGear(int gear) throws InvalidGearException {
		if (gear >= 0 && gear < GEAR_RANGES.length) {
			return GEAR_RANGES[gear];
		} else {
			throw new InvalidGearException("Invalid gear. The Dacia supernova does not have the gear " + gear);
		}
	}
}
