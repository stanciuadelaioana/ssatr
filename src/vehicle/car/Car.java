package vehicle.car;

import vehicle.Color;
import vehicle.Vehicle;

public abstract class Car extends Vehicle {

    public static final int NO_WHEELS = 4;

    public Car(Color color, int productionYear){
        super(color, productionYear);
    }

    @Override
    public int getNumberOfWheels() {
        return Car.NO_WHEELS;
    }
}
