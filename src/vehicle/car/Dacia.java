package vehicle.car;

import vehicle.Color;

public abstract class Dacia extends Car{

    public static final String PRODUCER = "Dacia";
    
    public Dacia(Color color, int productionYear){
        super(color, productionYear);
    }

    @Override
    public final String getProducer() {
        return Dacia.PRODUCER;
    }

}
