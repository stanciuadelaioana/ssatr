package vehicle.exceptions;

public class InvalidProductionYearException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2783406155051863321L;

	public InvalidProductionYearException(String string) {
		super(string);
	}

}
