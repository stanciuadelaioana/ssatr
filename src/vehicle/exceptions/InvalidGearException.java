package vehicle.exceptions;

public class InvalidGearException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -411659632012504052L;

	public InvalidGearException(String string) {
		super(string);
	}

}
