package vehicle.exceptions;

public class EngineNotStoppedException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8298092983821006184L;

	public EngineNotStoppedException(String string) {
		super(string);
	}

}
