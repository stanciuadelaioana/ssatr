package vehicle.exceptions;

public class InvalidSpeedException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2840283632085486910L;

	public InvalidSpeedException(String string) {
		super(string);
	}

}
