package vehicle.exceptions;

public class VehicleRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 946811603044620126L;

	public VehicleRuntimeException(String message, Exception cause) {
		super(message, cause);
	}

}
