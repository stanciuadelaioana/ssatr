package vehicle.exceptions;

public class InvalidColorException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2857674959733190720L;

	public InvalidColorException(String string) {
		super(string);
	}
}
