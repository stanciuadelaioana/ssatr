package vehicle.exceptions;

public class EngineNotStartedException extends VehicleException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2038572921323030222L;

	public EngineNotStartedException(String string) {
		super(string);
	}

}
