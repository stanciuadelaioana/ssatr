package vehicle.exceptions;

public class VehicleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4379910249256891779L;

	public VehicleException(String string) {
		super(string);
	}
}
