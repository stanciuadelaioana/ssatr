package vehicle.moto;

import vehicle.Color;

public abstract class Yamaha extends Motorcycle {

	public Yamaha(Color color, int productionYear) {
		super(color, productionYear);
	}

	public static final String PRODUCER = "Yamaha";

	@Override
	public final String getProducer() {
		return Yamaha.PRODUCER;
	}

}
