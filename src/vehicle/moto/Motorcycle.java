package vehicle.moto;

import vehicle.Color;
import vehicle.Vehicle;

public abstract class Motorcycle extends Vehicle {

    public Motorcycle(Color color, int productionYear) {
        super(color, productionYear);
    }

    public static final int NO_WHEELS = 2;

    @Override
    public int getNumberOfWheels() {
        return Motorcycle.NO_WHEELS;
    }

}
