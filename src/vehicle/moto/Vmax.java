package vehicle.moto;

import vehicle.Color;
import vehicle.exceptions.InvalidGearException;

public class Vmax extends Yamaha {

	public Vmax(Color color, int productionYear) {
		super(color, productionYear);
	}

	public static final int MAX_SPEED = 300;

	public static final String MODEL = "VMax";

	public static final int PRODUCTION_START_YEAR = 2000;

	public static final int PRODUCTION_END_YEAR = 2016;

	public static final int NUMBER_OF_GEARS = 5;

	private static final int[] GEAR_RANGES = { 0, 60, 120, 180, 250, 300 };

	@Override
	public int getMaxSpeed() {
		return Vmax.MAX_SPEED;
	}

	@Override
	public String getModel() {
		return Vmax.MODEL;
	}

	@Override
	public int getProductionStartYear() {
		return Vmax.PRODUCTION_START_YEAR;
	}

	@Override
	public int getProductionEndYear() {
		return Vmax.PRODUCTION_END_YEAR;
	}

	@Override
	public int getNumberOfGears() {
		return Vmax.NUMBER_OF_GEARS;
	}

	@Override
	public int getMaxSpeedForGear(int gear) throws InvalidGearException {

		if (gear >= 0 && gear < GEAR_RANGES.length) {
			return GEAR_RANGES[gear];
		} else {
			throw new InvalidGearException("Invalid gear. The Yamaha VMax does not have the gear " + gear);
		}
	}
}
