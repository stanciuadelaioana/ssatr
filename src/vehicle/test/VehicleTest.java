package vehicle.test;

import vehicle.Color;
import vehicle.Vehicle;
import vehicle.car.DaciaSuperNova;
import vehicle.exceptions.EngineNotStartedException;
import vehicle.exceptions.EngineNotStoppedException;
import vehicle.exceptions.InvalidGearException;
import vehicle.exceptions.InvalidSpeedException;
import vehicle.moto.Vmax;

public class VehicleTest {

	public static void main(String[] args) {
		Vehicle greenDacia = new DaciaSuperNova(Color.GREEN, 2012);
		Vehicle blueYamaha = new Vmax(Color.BLUE, 2002);
				
		try {
			greenDacia.startEngine();
			greenDacia.changeGear(1);
			greenDacia.accelerate(20);
			greenDacia.stopEngine();
			
		} catch (EngineNotStartedException e) {
			e.printStackTrace();
		} catch (InvalidGearException e) {
			e.printStackTrace();
		} catch (InvalidSpeedException e) {
			e.printStackTrace();
		} catch (EngineNotStoppedException e) {
			e.printStackTrace();
		}
	}
}
