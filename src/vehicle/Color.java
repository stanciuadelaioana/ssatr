package vehicle;

public enum Color {

	BLACK("black"), WHITE("white"), GREEN("green"), YELLOW("yellow"), BLUE("blue");

	private String name;

	Color(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
